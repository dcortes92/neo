# Running this template locally:

In order to run this in a server with python installed:

* Open a terminal and navigate to this folder
* Run `python -m SimpleHTTPServer` assuming that python is installed
* Open the browser at http://localhost:8080