$(document).ready(function() {
  /* Open a modal within a modal requires special logic */
  $('#openRegisterModal').on('click', function(e) {
    e.preventDefault();
    // close login modal
    $('#modalLogin').modal('hide');
    // wait for hide transitions to finish
    $("#modalLogin").on("hidden.bs.modal", function() {
      // after completely hiding login modal, open the register modal
      $("#modalRegister").modal("show");
    });
  });
});